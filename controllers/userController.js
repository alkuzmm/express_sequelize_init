const passport = require("passport");
const { User } = require("../models");
const logger = require("../helpers/logger");
const authUtils = require("../helpers/authUtils");

module.exports = {
  /**
   * Create new user method
   */
  async createUser(req, res, next) {
    try {
      const { login, username, email, password } = req.body;
      const userFromDb = await User.findOne({ where: { login } });
      if (userFromDb) {
        return res.status(400).json({ msg: "User already exists" });
      }
      const { salt, hash } = authUtils.genPassword(password);
      const newUser = await User.create({
        login,
        username,
        email,
        role: "GUEST",
        hash,
        salt,
      });
      return res.status(201).json(newUser);
    } catch (err) {
      logger.log({
        level: "error",
        message: "Error create new user",
        src: "userController/createUser",
        deatils: err,
      });
      return res.status(500).json({ msg: err.message });
    }
  },

  async loginUser(req, res, next) {
    try {
      const { login, password } = req.body;
      const user = await User.findOne({ where: { login } });

      if (!user) {
        res.status(401).json({
          success: false,
          msg: "Invalid credentials",
        });
      } else {
        const { hash, salt } = user.get({ plain: true });
        const isValid = authUtils.validPassword(password, hash, salt);

        if (!isValid) {
          res.status(401).json({
            success: false,
            message: "Invalid credentials",
          });
        } else {
          const { token, expires } = authUtils.issueJWT(user);
          res.status(200).json({ ...user.toJSON(), token, expiresIn: expires });
        }
      }
    } catch (err) {
      res.status(500).json({ status: false, msg: "Uknown error" });
    }
  },

  async allUsers(req, res, next) {
    try {
      const users = await User.findAll();
      const usersPlain = users.map((usr) => {
        return usr.toJSON();
      });
      res.status(200).json(usersPlain);
    } catch (error) {}
  },
};
