module.exports = {
  async testPage(req, res, next) {
    res.sendFile("index.html", { root: "./public" });
  },
};
