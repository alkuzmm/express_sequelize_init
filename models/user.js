"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    toJSON() {
      return { ...this.get(), id: undefined, salt: undefined, hash: undefined };
    }
  }
  User.init(
    {
      uuid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        unique: true,
      },
      login: {
        type: DataTypes.STRING(32),
        require: true,
        unique: true,
      },
      username: {
        type: DataTypes.STRING(32),
        require: true,
      },
      email: {
        type: DataTypes.STRING(32),
        validate: {
          isEmail: true,
        },
      },
      role: {
        type: DataTypes.ENUM("USER", "ADMIN", "GUEST"),
      },
      hash: {
        type: DataTypes.STRING,
        require: true,
        unique: true,
      },
      salt: {
        type: DataTypes.STRING,
        require: true,
        unique: true,
      },
    },
    {
      sequelize,
      tableName: "users",
      modelName: "User",
      timestamps: false,
    }
  );
  return User;
};
