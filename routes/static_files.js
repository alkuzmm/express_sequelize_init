const express = require("express");
const staticController = require("../controllers/staticController");
const router = express.Router();

module.exports = function (app) {
  router.route("/test-page").get((...params) => staticController.testPage(...params));

  /**
   * TODO: add route and function  user update
   */

  app.use("/static", router);
};
