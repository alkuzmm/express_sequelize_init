const express = require("express");
const router = express.Router();
const passport = require("passport");
const userController = require("../controllers/userController");

module.exports = function (app) {
  router
    .route("/register")
    .post((...params) => userController.createUser(...params));
  router
    .route("/login")
    .post((...params) => userController.loginUser(...params));
  router
    .route("/all")
    .get(passport.authenticate("jwt", { session: false }), (...params) =>
      userController.allUsers(...params)
    );
    /**
     * TODO: add route and function  user update
     */

  app.use("/users", router);
};
